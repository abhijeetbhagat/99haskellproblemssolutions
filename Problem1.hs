{-
Problem 1

(*) Find the last element of a list.

(Note that the Lisp transcription of this problem is incorrect.)

Example in Haskell:

Prelude> myLast [1,2,3,4]
4
Prelude> myLast ['x','y','z']
'z'
-}

import Test.HUnit

myLast (h:[]) = h
myLast (_:t)  = myLast t


main = do
	let test1 = TestCase(assertEqual "Checking myLast [1,2,3,4]: " 4 (myLast [1,2,3,4]))
	let test2 = TestCase(assertEqual "Checking myLast [1,2,3,4]: " 1 (myLast [1]))
	let tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2]
  	runTestTT tests
