{-
Drop every N'th element from a list.

Example:

* (drop '(a b c d e f g h i k) 3)
(A B D E G H K)

Example in Haskell:

*Main> dropEvery "abcdefghik" 3
"abdeghk"
-}

dropEvery::[a]->Int->[a]
dropEvery lst n = snd $ foldl step (1, []) lst
		        where step (c, acc) x | c < n = (c+1, acc ++ [x])
                                              | otherwise = (1, acc)

main = print $ dropEvery "abhijeet" 2
