{-
Problem 5

(*) Reverse a list.

Example in Haskell:

Prelude> myReverse "A man, a plan, a canal, panama!"
"!amanap ,lanac a ,nalp a ,nam A"
Prelude> myReverse [1,2,3,4]
[4,3,2,1]
-}

import Test.HUnit

myReverse::[a]->[a]->[a]
myReverse (h:[]) result = (h:result)
myReverse (h:t) result = myReverse t (h:result) 

main = do
	let test1 = TestCase $ assertEqual "Testing" "ihba" (myReverse "abhi" [])
	let test2 = TestCase $ assertEqual "Testing" [1,2,3,4] (myReverse [4,3,2,1] [])
	let tests = TestList [TestLabel "test1" test1,TestLabel "test2" test2]
	runTestTT tests		
