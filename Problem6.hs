{-
Find out whether a list is a palindrome. A palindrome can be read forward or backward; e.g. (x a m a x).

Example in Haskell:

*Main> isPalindrome [1,2,3]
False
*Main> isPalindrome "madamimadam"
True
*Main> isPalindrome [1,2,4,8,16,8,4,2,1]
True
-}

import Test.HUnit

isPalindrome::(Eq a)=>[a]->Bool
isPalindrome lst = lst == (reverse lst)



main = do
	let test1 = TestCase $ assertEqual "Testing" True (isPalindrome [1,2,1])
	let tests = TestList [TestLabel "test1" test1]
	runTestTT tests 		
