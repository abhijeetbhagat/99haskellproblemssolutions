{-
Problem 9

(**) Pack consecutive duplicates of list elements into sublists. If a list contains repeated elements they should be placed in separate sublists.

Example:

* (pack '(a a a a b c c a a d e e e e))
((A A A A) (B) (C C) (A A) (D) (E E E E))

Example in Haskell:

*Main> pack ['a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 
             'a', 'd', 'e', 'e', 'e', 'e']
["aaaa","b","cc","aa","d","eeee"]
-}

import Test.HUnit

pack::(Eq a)=>[a]->[[a]]
pack (h:t) = foldl step [[h]] t
	     where step acc x | (last (last acc)) == x = (init acc) ++ [(last acc) ++ [x]]
			      | otherwise	       = acc ++ [[x]]

main = do
       let test1 = TestCase $ assertEqual "Testing" ["aa", "b"] (pack "aab")
       let test2 = TestCase $ assertEqual "Testing" [[1,1],[2,2]] (pack [1,1,2,2])
       let tests = TestList [TestLabel "test1" test1,TestLabel "test2" test2]
       runTestTT tests
