{-
 Problem 12

(**) Decode a run-length encoded list.

Given a run-length code list generated as specified in problem 11. Construct its uncompressed version.

Example in Haskell:

P12> decodeModified 
       [Multiple 4 'a',Single 'b',Multiple 2 'c',
        Multiple 2 'a',Single 'd',Multiple 4 'e']
"aaaabccaadeeee"
-}

import Test.HUnit

data T a = M Int a | S a deriving Show


decode::(Eq a)=>[T a]->[a]
decode lst = foldl step [] lst
             where step acc x = acc ++ (check x)
					where check (M n c) = replicate n c
					      check (S c)   = [c]

main = do
	let test1 = TestCase $ assertEqual "Testing" "aaaabccaadeeee"  (decode [M 4 'a', S 'b', M 2 'c', M 2 'a', S 'd', M 4 'e'])
	let tests = TestList [TestLabel "test1" test1]
	runTestTT tests
