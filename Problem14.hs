{-
Duplicate the elements of a list.

Example:

* (dupli '(a b c c d))
(A A B B C C C C D D)

Example in Haskell:

> dupli [1, 2, 3]
[1,1,2,2,3,3]	
-}
import Test.HUnit

dupli::(Eq a)=>[a]->[a]
dupli      = foldl step []
		where step acc x = acc ++ (replicate 2 x)


main = do
	let test1 = TestCase $ assertEqual "Test" [1,1,2,2,3,3] (dupli [1,2,3])
	let test2 = TestCase $ assertEqual "Test" [1,1,2,2,2,2,3,3] (dupli [1,2,2,3])
	let tests = TestList [TestLabel "test1" test1,TestLabel "test2" test2]
	runTestTT tests
