{-
Problem 7

(**) Flatten a nested list structure.

Transform a list, possibly holding lists as elements into a `flat' list by replacing each list with its elements (recursively).

Example:

* (my-flatten '(a (b (c d) e)))
(A B C D E)

Example in Haskell:

We have to define a new data type, because lists in Haskell are homogeneous.

 data NestedList a = Elem a | List [NestedList a]

*Main> flatten (Elem 5)
[5]
*Main> flatten (List [Elem 1, List [Elem 2, List [Elem 3, Elem 4], Elem 5]])
[1,2,3,4,5]
*Main> flatten (List [])
[]

-}


import Test.HUnit

data NestedList a = E a | L [NestedList a]  deriving Show

flatten::[NestedList a]->[a]
flatten = foldr step []
	   where step (E x) acc = x:acc
		 step (L x) acc = foldr step acc x

main = do
	let test1 = TestCase(assertEqual "Testing flatten [E 1, L[E 2], L[L[E 3]]]" [1,2,3] (flatten [E 1, L[E 2], L[L[E 3]]]))
	let test2 = TestCase(assertEqual "Testing flatten []" [] (flatten [L[E 1]]))
 	let tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2]
	runTestTT tests
