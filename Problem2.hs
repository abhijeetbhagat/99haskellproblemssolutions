{-
Problem 2

(*) Find the last but one element of a list.

(Note that the Lisp transcription of this problem is incorrect.)

Example in Haskell:

Prelude> myButLast [1,2,3,4]
3
Prelude> myButLast ['a'..'z']
'y'
-}

import Test.HUnit

myButLast (x:_:[]) = x
myButLast (_:y:z)  = myButLast (y:z)

main = do
        let test1 = TestCase(assertEqual "Checking myButLast [1,2,3,4,5]: " 4 (myButLast [1,2,3,4,5]))
	let test2 = TestCase(assertEqual "Checking myButLast [1,2,3,4,5]: " 1 (myButLast [1,2,3,4,5]))
	let test3 = TestCase(assertEqual "Checking myButLast [1,2]" 1 (myButLast [1,2]))
	let tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2, TestLabel "test3" test3]
	runTestTT tests