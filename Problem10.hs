
encode::(Eq a)=>[a]->[(Int,a)]
encode (h:t) = foldl step [(1, h)] t
		where step acc x = (init acc) ++ (check (last acc))
						 where check (n,y) | x == y = [(n+1, y)]
								   | otherwise = [(n,y), (1,x)]
main = print $ encode "aabbhhiia"
