compress::(Eq a)=>[a]->[a]
compress (h:t) = foldl step [h] t
		 where step acc x | (last acc) == x = acc
				  | otherwise 	    = acc ++ [x]

main = print $ compress [1,1,2,3,4,1]
