{-
Extract a slice from a list.

Given two indices, i and k, the slice is the list containing the elements between the i'th and k'th element of the original list (both limits included). Start counting the elements with 1.

Example:

* (slice '(a b c d e f g h i k) 3 7)
(C D E F G)

Example in Haskell:

*Main> slice ['a','b','c','d','e','f','g','h','i','k'] 3 7
 "cdefg"
-}

import Test.HUnit

slice::[a]->Int->Int->[a]
slice lst si ei = snd $ foldl step (1, []) lst
                        where step (c, acc) x | c < si || c > ei  = (c+1, acc)
                                              | otherwise = (c+1, acc ++ [x])

main = do
       let test1 = TestCase $ assertEqual "Test" "cdefg" (slice ['a','b','c','d','e','f','g','h','i','k'] 3 7)--print $ slice [1,2,3,4,5] 2 4
       let tests = TestList [TestLabel "test1" test1]
       runTestTT tests
