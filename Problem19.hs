{-
Rotate a list N places to the left.

Hint: Use the predefined functions length and (++).

Examples:

* (rotate '(a b c d e f g h) 3)
(D E F G H A B C)

* (rotate '(a b c d e f g h) -2)
(G H A B C D E F)

Examples in Haskell:

*Main> rotate ['a','b','c','d','e','f','g','h'] 3
"defghabc"
 
*Main> rotate ['a','b','c','d','e','f','g','h'] (-2)
"ghabcdef"
-}

import Test.HUnit

slice::[a]->Int->Int->[a]
slice lst si ei = snd $ foldl step (1, []) lst
                        where step (c, acc) x | c < si || c > ei  = (c+1, acc)
                                              | otherwise = (c+1, acc ++ [x])

                                                     

rotate::[a]->Int->[a]
rotate lst n = (drop getn lst) ++ (slice lst 1 getn)
               where getn | n < 0 = ((length lst + n))
                          | otherwise = n

main::IO()
main = do
  print $ rotate [1..5] (-3)
