data T a = S a | M Int a deriving Show

encodeModified::(Eq a)=>[a]->[T a]
encodeModified (h:t) = foldl step [S h] t
			where step acc x = (init acc) ++ (check (last acc))
							 where check (S c) | x == c = [M 2 c]
									   |otherwise = [S c, S x]

							       check (M n c) | x == c = [M (n+1) c]
									     |otherwise = [M n c, S x] 


main = print $ encodeModified [1,1,2,3,4,4]
