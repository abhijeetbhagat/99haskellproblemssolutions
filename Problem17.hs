{-
Split a list into two parts; the length of the first part is given.

Do not use any predefined predicates.

Example:

* (split '(a b c d e f g h i k) 3)
( (A B C) (D E F G H I K))

Example in Haskell:

*Main> split "abcdefghik" 3
("abc", "defghik")
-}

split::[a]->Int->([a],[a])
split lst n = snd $ foldl step (1, ([], [])) lst
                    where step (c, (acc1, acc2)) x | c <= n = (c+1, (acc1 ++ [x], acc2))
                                                   | otherwise = (c, (acc1, acc2 ++ [x]))


main = print $ split "abhijeet" 4
