{-
Problem 4

(*) Find the number of elements of a list.

Example in Haskell:

Prelude> myLength [123, 456, 789]
3
Prelude> myLength "Hello, world!"
13

-}

import Test.HUnit

myLength::[a]->Int->Int
myLength (_:[]) cnt = cnt + 1
myLength (_:t) cnt = myLength t (cnt + 1)


main = do
	let test1 = TestCase $ assertEqual "Testing" 4 (myLength [1,2,3,4] 0)
	let test2 = TestCase $ assertEqual "Testing" 1 (myLength [1] 0)
	let tests = TestList [TestLabel "test1" test1,TestLabel "test2" test2]
	runTestTT tests
