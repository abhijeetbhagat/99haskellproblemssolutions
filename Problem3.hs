{-
Problem 3

(*) Find the K'th element of a list. The first element in the list is number 1.

Example:

* (element-at '(a b c d e) 3)
c

Example in Haskell:

Prelude> elementAt [1,2,3] 2
2
Prelude> elementAt "haskell" 5
'e'
-}


import Test.HUnit


elementAt'::[a]->Int->Int->Int->Maybe a
elementAt' (h:[]) i cnt len | i == cnt = Just h
                            | otherwise = Nothing

elementAt' (h:t)  i cnt len | i == cnt = Just h
                            | i < len = elementAt' t i (cnt+1) len
                            | otherwise = Nothing

elementAt::[a]->Int->Maybe a
elementAt lst i = if i < 1 then Nothing else elementAt' lst i 1 (length lst)

main = do 
	let test1 = TestCase(assertEqual "Testing elementAt [1,2,3,4] 3: " (Just 3) (elementAt [1,2,3,4] 3))
	let test2 = TestCase(assertEqual "Testing elementAt [1,2,3,4] 0: " Nothing  (elementAt [1,2,3,4] 0))
	let test3 = TestCase(assertEqual "Testing elementAt [1,2,3,4] 4: " Nothing  (elementAt [1,2,3,4] 4))
	let tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2, TestLabel "test3" test3]
	runTestTT tests
